package com.kshrd.intentfilterreciever;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView tvLabel = (TextView) findViewById(R.id.tvLabel);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            tvLabel.setText(bundle.getString("USERNAME"));
        }

    }
}
